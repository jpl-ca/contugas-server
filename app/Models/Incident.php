<?php

namespace Contugas\Models;

use Illuminate\Database\Eloquent\Model;
use Contugas\Models\IncidentType;

class Incident extends Model
{
    protected $fillable = ['user_id', 'incident_type_id', 'comment', 'lat', 'lng', 'address'];
    
    public function type()
    {
    	return $this->belongsTo(IncidentType::class, 'incident_type_id', 'id');
    }
}
