<?php

namespace Contugas\Models;

use Illuminate\Http\Request;
use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Carbon\Carbon;
use \Authorizer;
use Contugas\Libraries\OAuth;
use Contugas\Libraries\Timx;
use Contugas\Models\GasConsumption;

class User extends Model implements AuthenticatableContract,
                                    AuthorizableContract,
                                    CanResetPasswordContract
{
    use Authenticatable, Authorizable, CanResetPassword;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'client_number', 'id_number', 'email', 'password'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    public static function oAuth()
    {
        if(Authorizer::getResourceOwnerType() == 'user')
        {
            return static::find(Authorizer::getResourceOwnerId());
        }
        return null;
    }

    public static function me()
    {
        return array_merge( static::oAuth()->toArray(), ['access_token' => OAuth::getAccessToken()] );
    }

    public function gasConsumptions()
    {
        return $this->hasMany(GasConsumption::class, 'user_id', 'id');
    }

    public function gasConsumptionLastMonths($months = 6)
    {
        $gasConsumptionsCollection = $this->gasConsumptions()->orderBy('date', 'desc')->take($months)->get();
        $gasConsumptions = array();
        foreach ($gasConsumptionsCollection as $gasConsumption) {
            array_push( $gasConsumptions, $gasConsumption->toArray(true) );
        }
        $currentDate = Carbon::now()->format('Y-m-d h:i:s');
        $currentGasConsumption = [
            'user_id' => $this->id,
            'consumption' => 0,
            'amount_paid' => 0,
            'amount_saved' => 0,
            'month' => Carbon::now()->format('m'),
            'month_name' => Timx::spanishDate($currentDate),
            'year' => Carbon::now()->format('Y')
        ];
        $gasConsumptions = array_reverse($gasConsumptions);
        array_push($gasConsumptions, $currentGasConsumption);
        $gasConsumptions = array_reverse($gasConsumptions);
        return $gasConsumptions;// array_push($gasConsumptions, $currentGasConsumption);
    }
}
