<?php

namespace Contugas\Models\OAuth;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;
use Contugas\Models\OAuth\OauthAccessToken;
use Contugas\Libraries\OAuth;

class OauthSession extends Model
{
	protected static function boot() {
	    parent::boot();    
	    static::deleting(function(OauthSession $session){
	    	$session->accessToken->delete();
	    });
	}

    public function accessToken()
    {
    	return $this->hasOne(OauthAccessToken::class, 'session_id', 'id');
    }
}
