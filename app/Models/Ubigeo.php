<?php

namespace Contugas\Models;

use Illuminate\Database\Eloquent\Model;

class Ubigeo extends Model
{

    protected $table = 'ubigeo';

    protected $primaryKey = 'code';

    public $timestamps = false;

    public function provinces()
    {
    	return $this->hasMany(Ubigeo::class, 'parent', 'code');
    }

    public function districts()
    {
    	return $this->hasMany(Ubigeo::class, 'parent', 'code');
    }
}
