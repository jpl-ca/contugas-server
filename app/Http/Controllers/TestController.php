<?php

namespace Contugas\Http\Controllers;

use Illuminate\Http\Request;
use Contugas\Http\Requests;
use Contugas\Http\Controllers\Controller;
use Contugas\Models\Ubigeo;
use \Authorizer;

class TestController extends Controller
{
    public function getCase1(Request $request)
    {
    	$ubigeo = Ubigeo::whereNull('parent')->with('provinces.districts')->get();
        return responseJsonOk(['data' => $ubigeo]);
    }

    public function getCase2(Request $request)
    {
        return 'caca';
    }
}
