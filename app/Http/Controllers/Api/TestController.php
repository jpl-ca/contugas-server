<?php

namespace Contugas\Http\Controllers\Api;

use Illuminate\Http\Request;
use Contugas\Http\Requests;
use Contugas\Http\Controllers\Controller;

class TestController extends Controller
{
    public function getCase1(Request $request)
    {
        return responseJsonOk(['message' => 'hola']);
    }
}
