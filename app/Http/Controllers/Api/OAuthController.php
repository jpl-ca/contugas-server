<?php

namespace Contugas\Http\Controllers\Api;

use Illuminate\Http\Request;
use Contugas\Http\Requests;
use Contugas\Http\Controllers\Controller;
use Contugas\Libraries\OAuth;
use \Authorizer;
use Contugas\Models\User;
use Contugas\Models\OAuth\OauthAccessToken;

class OAuthController extends Controller
{
    public function __construct()
    {
        $this->middleware('oauth', ['except' => ['postIndex']]);
    }

    public function postIndex(Request $request)
    {
        $accessData = Authorizer::issueAccessToken();
        $userData = User::find(OauthAccessToken::find($accessData['access_token'])->session->owner_id)->toArray();
        return response()->json(array_merge($accessData, $userData));
    }

    public function deleteIndex(Request $request)
    {
    	OAuth::expireRequestSession();
        return responseJsonOk(['message' => 'sesión cerrada']);
    }

}
