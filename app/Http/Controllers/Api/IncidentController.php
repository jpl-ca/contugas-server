<?php

namespace Contugas\Http\Controllers\Api;

use Illuminate\Http\Request;
use Contugas\Http\Requests;
use Contugas\Http\Controllers\Controller;
use Contugas\Libraries\OAuth;
use Daylight\Routing\ApiCrudFunctions;
use \Contugas\Models\User;
use \Contugas\Models\Incident;

class IncidentController extends Controller
{
    use ApiCrudFunctions;
        
    protected $rules = [
        'user_id' => 'null_or_exists:users,id',
        'incident_type_id' => 'required|exists:incident_types,id',
        'comment' => 'required|min:50|max:255',
        'lat' => 'required|numeric',
        'lng' => 'required|numeric',
        'address' => 'required',
    ];

    protected $errorMsg = 'Se han encontrado errores en los datos';

    protected $okMsg = 'Registro creado correctamente';

    public function getCreate()
    {
        return responseJsonOk([]);
    }

    public function postIndex(Request $request)
    {
        if(OAuth::getAccessToken() !== null)
        {
            $request->merge(array('user_id' => User::oAuth()->id));
        }
        return $this->createOrFail(Incident::class, $request);
    }
}
