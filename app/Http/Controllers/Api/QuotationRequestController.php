<?php

namespace Contugas\Http\Controllers\Api;

use Illuminate\Http\Request;
use Contugas\Http\Requests;
use Contugas\Http\Controllers\Controller;
use Contugas\Libraries\OAuth;
use Daylight\Routing\ApiCrudFunctions;
use \Contugas\Models\Ubigeo;
use \Contugas\Models\QuotationRequest;

class QuotationRequestController extends Controller
{
    use ApiCrudFunctions;
        
    protected $rules = [
        'name' => 'required|max:255',
        'last_name' => 'max:255',
        'address' => 'required|max:255',
        'province_id' => 'required|exists:ubigeo,code',
        'phone' => 'required|alpha_dash|max:15',
        'email' => 'required|email',
        'comment' => 'required|string|min:50|max:255',
    ];

    protected $errorMsg = 'Se han encontrado errores en los datos';

    protected $okMsg = 'Registro creado correctamente';

    public function getCreate()
    {
        $provinces = Ubigeo::whereIn('code', ['1001', '1002', '1003', '1004'])->get()->toArray();
        return responseJsonOk([
            'message' => 'Datos para la creación de solicitud de cotización obtenidos correctamente',
            'data' => [
                'provinces' => $provinces
            ]
        ]);
    }

    public function postIndex(Request $request)
    {
        return $this->createOrFail(QuotationRequest::class, $request);
    }
}
