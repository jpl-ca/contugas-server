<?php

namespace Contugas\Http\Controllers\Api;

use Illuminate\Http\Request;
use Contugas\Http\Requests;
use Contugas\Http\Controllers\Controller;
use Contugas\Models\User;

class AccountController extends Controller
{
    public function getIndex(Request $request)
    {
        return responseJsonOk( User::me($request) );
    }
}
