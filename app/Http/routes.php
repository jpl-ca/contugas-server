<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

use \Authorizer;
use Contugas\Models\User;
use Contugas\Models\OAuth\OauthClient;
use Contugas\Models\OAuth\OauthAccessToken;
use Contugas\Models\OAuth\OauthSession;

Route::group(['namespace' => 'Api', 'prefix' => 'api'], function(){

	//Route::controller('oauth', 'OAuthController');

	Route::controller('incident', 'IncidentController');

	Route::controller('quotation-request', 'QuotationRequestController');

	Route::group(['prefix' => 'oauth'], function(){

		Route::group(['prefix' => 'me', 'middleware' => 'oauth'], function(){

			Route::controller('incident', 'IncidentController');

			Route::controller('gas-consumption', 'GasConsumptionController');

			Route::controller('/', 'AccountController');

		});

		Route::controller('/', 'OAuthController');

	});

	Route::group(['middleware' => 'oauth'], function(){

		Route::controller('test', 'TestController');

	});
	//Route::controller('auth', 'AuthController',
    //            ['only' => ['index', 'show']]);
});

Route::controller('test', 'TestController');

Route::get('/create-oauth-client', function(){
	$client = OauthClient::find('contugas_android');

	if(is_null($client))
	{
		$client = OauthClient::newClient('contugas_android', 'Contugas Android Client');
	}

	return $client->toArray();
});

Route::get('/create-user', function(){
	return User::create([
		'name' => 'Luis Sanchez',
		'client_number' => 852963,
		'id_number' => 44556677,
		'email' => str_random(10) . '@api.contugas.com',
		'password' => \Hash::make('password'),
	])->toArray();
});

Route::post('oauth/access_token', function() {
    return Response::json(Authorizer::issueAccessToken());
});

Route::get('/', function(){

	setlocale(LC_ALL,"es_ES.UTF-8");
	$string = "24/11/2014";
	$date = DateTime::createFromFormat("d/m/Y", $string);
	echo strftime("%B",$date->getTimestamp());
});