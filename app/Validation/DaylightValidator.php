<?php

namespace Contugas\Validation;

use Illuminate\Validation\Validator;

class DaylightValidator extends Validator
{
    // Laravel uses this convention to look for validation rules, this function will be triggered 
    // for composite_unique
    public function validateNullOrExists( $attribute, $value, $parameters )
    {
        if ($value == null)
            return true;
        else
            return $this->validateExists($attribute, $value, $parameters);
    }
}