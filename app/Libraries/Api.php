<?php

namespace Contugas\Libraries;

use Illuminate\Http\Request;
use Cookie;

class Api {

	protected static $_sessionTokenName = 'contugas_session_token';

	protected static $_validKeys = [
		'android@contugas'	=> '^Zyba07md1MZGNZA3p^|Ac2*6G2fI-y3Q4GaRZ%lg^T%=0UlokK2Lh8MVWhC',	// Android
		'web@contugas'		=> 'nX2KeBfZ9+qCe-yYvJRd|~AiKrZJXqHoM^2q_H2Yn++cWny3%k0n*m8iQvEL'	// Web
	];

	protected static $_validDeviceTypes = [
		'contugas-android'
	];

	public static function validateData(Request $request)
	{
		return 
			isset(self::$_validKeys[$request->get('client_id', '1')]) &&
			self::$_validKeys[$request->get('client_id', '1')] == $request->get('client_secret', '1');
	}

	public static function validateDeviceType(Request $request)
	{
		return in_array($request->header('Device-Type', '1'), self::$_validDeviceTypes);
	}

	public static function validateToken(Request $request)
	{
		return $request->cookie('contugas_session_token') != null;
	}

	public static function destroyToken()
	{
		return Cookie::forget(self::$_sessionTokenName);
	}

	public static function createToken()
	{
		return Cookie::forever(self::$_sessionTokenName, str_random(60));
	}

}