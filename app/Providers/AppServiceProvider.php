<?php

namespace Contugas\Providers;

use Illuminate\Support\ServiceProvider;
use Contugas\Validation\DaylightValidator;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
