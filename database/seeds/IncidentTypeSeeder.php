<?php

use Illuminate\Database\Seeder;

class IncidentTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$date = \Carbon\Carbon::now()->toDateTimeString();

		DB::table('incident_types')->insert(array(
			array('name' => 'Falta de servicio', 'created_at' => $date, 'updated_at' => $date),
			array('name' => 'Fuga de gas', 'created_at' => $date, 'updated_at' => $date),
			array('name' => 'Tubería rota', 'created_at' => $date, 'updated_at' => $date)
		));
    }
}
