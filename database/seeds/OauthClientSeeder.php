<?php

use Illuminate\Database\Seeder;
use Contugas\Models\OAuth\OauthClient;

class OauthClientSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $androidClient = OauthClient::find('contugas_android');

        if(is_null($androidClient))
        {
            $androidClient = OauthClient::newClient('contugas_android', 'Contugas Android Client', '8lPZnWyF2KCOjy3WGIdWRzCov54eMa34J1SB0OD9');
        }

        $webClient = OauthClient::find('contugas_web');

        if(is_null($webClient))
        {
            $webClient = OauthClient::newClient('contugas_web', 'Contugas Web Client', 'dWRkjS354eMa34SA670OD9332ZnWyF2KCOjy3WGI');
        }
    }
}
